/**
 * Copyright 2021 Huawei Technologies Co., Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef MINDSPORE_LITE_SRC_RUNTIME_DELEGATE_NNAPI_DELEGATE_H_
#define MINDSPORE_LITE_SRC_RUNTIME_DELEGATE_NNAPI_DELEGATE_H_

#include <vector>
#include <map>
#include <string>
#include <memory>
#include "include/api/delegate.h"
#include "include/context.h"
// #include "src/delegate/npu/npu_manager.h"
// #include "src/delegate/npu/pass/npu_pass_manager.h"
// #include "src/delegate/npu/op/npu_op.h"
// #include "src/delegate/tensorrt/tensorrt_subgraph.h"
#include "src/delegate/nnapi/nnapi_subgraph.h"
#include "src/delegate/nnapi/op/nnapi_op.h"
#include "include/api/kernel.h"
#include "include/errorcode.h"
#include "src/common/log_adapter.h"
#include "include/api/context.h"

namespace mindspore::lite {
typedef NNAPIOp *(*NNAPIGetOp)(const schema::Primitive *primitive,
                               const std::vector<mindspore::MSTensor> &in_tensors,
                               const std::vector<mindspore::MSTensor> &out_tensors, 
                               const std::string &name);
                                     
class NNAPIDelegate : public Delegate {
 public:
//   explicit NNAPIDelegate(lite::NpuDeviceInfo device_info) : Delegate() { frequency_ = device_info.frequency_; }
  // init NNAPI config, 
  
  explicit NNAPIDelegate(mindspore::Context *context) : context_(context) {}
  

  ~NNAPIDelegate() override =default;

  // check whether hardware supports framework
  // apply and init relevant resources 
  int Init() override;

  // analyze each op, check whether this framework support this op
  // for a continuous sequence of supported ops, build a subgraph to replace the sequence
  int Build(DelegateModel *model) override;

 protected:
  NNAPIOp *FindNNAPIOp(kernel::Kernel *kernel, const schema::Primitive *primitive);

  NNAPISubGraph *CreateNNAPIGraph(const std::vector<NNAPIOp *> &ops, DelegateModel *model, KernelIter from,
                                        KernelIter end);

  std::map<schema::PrimitiveType, NNAPIGetOp> op_func_lists_;

  mindspore::Context *context_;
//   NPUOp *GetOP(kernel::Kernel *kernel, const schema::Primitive *primitive);

//   kernel::Kernel *CreateNPUGraph(const std::vector<NPUOp *> &ops, DelegateModel *model, KernelIter from,
//                                  KernelIter end);

//   NPUManager *npu_manager_ = nullptr;
//   NPUPassManager *pass_manager_ = nullptr;
//   std::map<schema::PrimitiveType, NPUGetOp> op_func_lists_;
//   int frequency_ = 0;
};
}  // namespace mindspore::lite

#endif  // MINDSPORE_LITE_SRC_RUNTIME_DELEGATE_NNAPI_DELEGATE_H_
