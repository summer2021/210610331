/**
 * Copyright 2021 Huawei Technologies Co., Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef MINDSPORE_LITE_SRC_RUNTIME_DELEGATE_NNAPI_SUB_GTAPH_
#define MINDSPORE_LITE_SRC_RUNTIME_DELEGATE_NNAPI_SUB_GTAPH_
#include <utility>
#include <set>
#include <string>
#include <vector>
#include <memory>
#include "include/api/kernel.h"
#include <android/NeuralNetworks.h>
// #include "src/delegate/tensorrt/tensorrt_runtime.h"
// #include "src/delegate/tensorrt/tensorrt_utils.h"
#include "include/api/context.h"
#include "src/delegate/nnapi/op/nnapi_op.h"


namespace mindspore::lite {
class NNAPISubGraph : public kernel::Kernel {
 public:
  NNAPISubGraph(std::vector<NNAPIOp *> ops, 
                const std::vector<mindspore::MSTensor> &inputs,
                const std::vector<mindspore::MSTensor> &outputs,
                const mindspore::Context *ctx,
                std::shared_ptr<GPUDeviceInfo> device_info)
      : kernel::Kernel(inputs, outputs, nullptr, ctx), all_ops_(std::move(ops)), device_info_(device_info) {
    // trt_specific_weight_nodes_ = {
    //   schema::PrimitiveType_Conv2DFusion, schema::PrimitiveType_ReduceFusion, schema::PrimitiveType_Transpose,
    //   schema::PrimitiveType_Gather,       schema::PrimitiveType_Reshape,      schema::PrimitiveType_PowFusion,
    //   schema::PrimitiveType_DivFusion,    schema::PrimitiveType_MatMul,       schema::PrimitiveType_ScaleFusion};
  }

  ~NNAPISubGraph() override;

  // called at LiteSession::CreateGraph, implemented some input-data-agnostic operations like rearranging constant weight tensor
  int Prepare() override;

  // get input data from in_tensors; execute inference; write results to output_tensors
  int Execute() override;

  // dynamically reshape
  int ReSize() override {
    MS_LOG(ERROR) << "NNAPI does not support the resize function temporarily.";
    return RET_ERROR;
  }

  int Init();
  int BuildNNAPIGraph();

protected:
  std::vector<NNAPIOp *> all_ops_{};
  std::shared_ptr<GPUDeviceInfo> device_info_{nullptr};


  //TODO: below all to be deleted 
  ANeuralNetworksModel* model = nullptr;
  ANeuralNetworksMemory* input_tensor;
  ANeuralNetworksMemory* output_tensor;

  int input_tensor_size;
  int output_tensor_size;
  int output_tensor_fd;
  void *output_tensor_mutable;

  int output_operand_index; 
  
};

} // namespace mindspore::lite
#endif  // MINDSPORE_LITE_SRC_RUNTIME_DELEGATE_NNAPI_SUB_GTAPH_