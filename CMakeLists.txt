# include_directories(${TENSORRT_PATH}/include)
# set(NNAPI_LIB_PATH $ENV{ANDROID_NDK}/platforms/android-29/arch-arm64/usr/lib)
include_directories($ENV{ANDROID_NDK}/sysroot/usr/include)
file(GLOB_RECURSE NNAPI_RUNTIME_SRC
        ${CMAKE_CURRENT_SOURCE_DIR}/*.cc
        ${CMAKE_CURRENT_SOURCE_DIR}/op/*.cc
        )
add_library(libneuralnetworks SHARED IMPORTED)
set_target_properties(libneuralnetworks PROPERTIES IMPORTED_LOCATION
        ${NNAPI_LIB_PATH}/libneuralnetworks.so)

add_library(libandroid SHARED IMPORTED)
set_target_properties(libandroid PROPERTIES IMPORTED_LOCATION
        ${NNAPI_LIB_PATH}/libandroid.so)

add_library(nnapi_kernel_mid OBJECT ${NNAPI_RUNTIME_SRC})
add_dependencies(nnapi_kernel_mid fbs_src)
target_link_libraries(
        nnapi_kernel_mid
        libneuralnetworks
        libandroid
)
