/**
 * Copyright 2021 Huawei Technologies Co., Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "src/delegate/nnapi/op/convolution_nnapi.h"

namespace mindspore::lite {
  
//   ConvolutionNNAPI::~ConvolutionNNAPI() {}
  int ConvolutionNNAPI::IsSupport(const schema::Primitive *primitive, const std::vector<mindspore::MSTensor> &  in_tensors, const std::vector<mindspore::MSTensor> &out_tensors) {
    return RET_OK;
  }

  int ConvolutionNNAPI::AddOperand(ANeuralNetworksModel* model, int *operand_cnt) {
    // consider changing void to int to include error code 
    // const Conv2DFusion *value_as_Conv2DFusion() const {
    // return value_type() == PrimitiveType_Conv2DFusion ? static_cast<const Conv2DFusion *>(value()) : nullptr;
    const schema::Conv2DFusion* conv_op = op_primitive_->value_as_Conv2DFusion();
    if (conv_op == nullptr) {
      MS_LOG(ERROR) << "op action convertion failed";
      return RET_ERROR;
    }

    /*
    all values needed to define the inputs and output of an NNAPI Conv2D layer:

    input tensor's:
    batches
    height
    width
    depth_in (num_channels, or in_channels)

    filter's:
    depth_out (out_channels)
    filter_height
    filter_width

    bias
    
    for explicit_padding: lengths
    left
    right
    top
    bottom

    stride:
    width stride
    height stride

    activation:
    Fusecode

    dilation:
    width dilation
    height dilation
    */


    /*
    auto group = static_cast<int>(conv_prim->group());
    auto stride_h = static_cast<int>(*(conv_prim->stride()->begin()));
    auto stride_w = static_cast<int>(*(conv_prim->stride()->begin() + 1));
    auto dilation_h = static_cast<int>(*(conv_prim->dilation()->begin()));
    auto dilation_w = static_cast<int>(*(conv_prim->dilation()->begin() + 1));
    conv_->set_attr_strides(ge::AttrValue::LIST_INT({stride_h, stride_w}));
    conv_->set_attr_dilations(ge::AttrValue::LIST_INT({dilation_h, dilation_w}));
    conv_->set_attr_groups(group);

    if (conv_prim->pad_mode() == schema::PadMode_SAME) {
      conv_->set_attr_pad_mode(ge::AttrValue::STR{"SAME"});
      conv_->set_attr_pads(ge::AttrValue::LIST_INT({0, 0, 0, 0}));
    } else if (conv_prim->pad_mode() == schema::PadMode_VALID) {
      conv_->set_attr_pad_mode(ge::AttrValue::STR{"VALID"});
      conv_->set_attr_pads(ge::AttrValue::LIST_INT({0, 0, 0, 0}));
    } else {
      conv_->set_attr_pad_mode(ge::AttrValue::STR{"SPECIFIC"});
      auto pad_u = static_cast<int>(*(conv_prim->pad_list()->begin() + PAD_UP));
      auto pad_d = static_cast<int>(*(conv_prim->pad_list()->begin() + PAD_DOWN));
      auto pad_l = static_cast<int>(*(conv_prim->pad_list()->begin() + PAD_LEFT));
      auto pad_r = static_cast<int>(*(conv_prim->pad_list()->begin() + PAD_RIGHT));
      conv_->set_attr_pads(ge::AttrValue::LIST_INT({pad_u, pad_d, pad_l, pad_r}));
    }
    return RET_OK;
    */

    // get input tensor info: to be obtained from the input tensor of this op, not from the primitives
    // in_tensors_[0] is the REAL input tensor of Conv2D op
    // in_tensors_[1] and [2] are weight and bias respectively
    const vector<int64_t> &input_shape = in_tensors_[0].Shape();
    int batches = input_shape[0]; // MS Conv2D has only 1 input tensor, thus in_tensors_[0]
    int height = input_shape[1];
    int width = input_shape[2];
    int depth_in = input_shape[3];  

    // get filter info 
    mindspore::MSTensor &filter = in_tensors_[1];
    const vector<int64_t> &filter_shape = filter.Shape();
    int depth_out = filter_shape[0];  //depth_out is also the length of the 1D bias tensor
    int filter_height = filter_shape[1];
    int filter_width = filter_shape[2];
    // int depth_in = filter_shape[3];

    // get bias info: omitted, since depth_out is also the length of the 1D bias tensor

    // get padding info:
    // int pad_u = -1;
    // int pad_d = -1;
    // int pad_l = -1;
    // int pad_r = -1;
    // int pad_code = -1;
    bool is_explicit_padding = true;

    // case 1: explicit padding
    if (conv_op->pad_mode() != schema::PadMode_SAME && conv_op->pad_mode() != schema::PadMode_VALID) {
      this->pad_u = static_cast<int>(*(conv_op->pad_list()->begin() + 0));
      this->pad_d = static_cast<int>(*(conv_op->pad_list()->begin() + 1));
      this->pad_l = static_cast<int>(*(conv_op->pad_list()->begin() + 2));
      this->pad_r = static_cast<int>(*(conv_op->pad_list()->begin() + 3));
    }

    // case 2: implicit padding
    else if (conv_op->pad_mode() == schema::PadMode_SAME) {
      this->pad_code = ANEURALNETWORKS_PADDING_SAME;
      is_explicit_padding = false;
    }
    else {
      this->pad_code = ANEURALNETWORKS_PADDING_VALID;
      is_explicit_padding = false;
    }
    this->is_explicit_padding = is_explicit_padding;
  
    // get stride: width and height
    this->stride_width = static_cast<int>(*(conv_op->stride()->begin() + 1));
    this->stride_height = static_cast<int>(*(conv_op->stride()->begin()));
    
    // get fuse code
    // int fuse_code;
    auto act_type = conv_op->activation_type();
   
    if (act_type == schema::ActivationType_NO_ACTIVATION)
      this->fuse_code = ANEURALNETWORKS_FUSED_NONE;
    else if (act_type == schema::ActivationType_RELU)
      this->fuse_code = ANEURALNETWORKS_FUSED_RELU;
    else if (act_type == schema::ActivationType_RELU1)
      this->fuse_code = ANEURALNETWORKS_FUSED_RELU1;
    else if (act_type == schema::ActivationType_RELU6)
      this->fuse_code = ANEURALNETWORKS_FUSED_RELU6;
    else {
      MS_LOG(ERROR) << "NNAPI does not support this fused activation type!";
      return RET_ERROR;
    }

    // NCHW false
    // this->is_NCHW = false;

    // get dilation: width and height
    this->dilation_width = static_cast<int>(*(conv_prim->dilation()->begin() + 1));
    this->dilation_height = static_cast<int>(*(conv_prim->dilation()->begin()));

    // get output tensor info
    const vector<int64_t> &output_shape = out_tensors_[0].Shape();
    //int batches = output_shape[0]; 
    int out_height = output_shape[1];
    int out_width = output_shape[2];
    // int depth_in = output_shape[3];  

    /*
    using the above params to set up NNAPI operand type
    */
    ANeuralNetworksOperandType input_type;
    input_type.type = ANEURALNETWORKS_TENSOR_FLOAT32;
    input_type.scale = 0.f;    // These fields are used for quantized tensors
    input_type.zeroPoint = 0;  // These fields are used for quantized tensors
    input_type.dimensionCount = 4;
    uint32_t input_type_dims[4] = {batches, height, width, depth_in};
    input_type.dimensions = input_type_dims;

    ANeuralNetworksOperandType filter_type;
    filter_type.type = ANEURALNETWORKS_TENSOR_FLOAT32;
    filter_type.scale = 0.f;    // These fields are used for quantized tensors
    filter_type.zeroPoint = 0;  // These fields are used for quantized tensors
    filter_type.dimensionCount = 4;
    uint32_t filter_type_dims[4] = {depth_out, filter_height, filter_width, depth_in};
    filter_type.dimensions = filter_type_dims;

    ANeuralNetworksOperandType bias_type;
    bias_type.type = ANEURALNETWORKS_TENSOR_FLOAT32;
    bias_type.scale = 0.f;    // These fields are used for quantized tensors
    bias_type.zeroPoint = 0;  // These fields are used for quantized tensors
    bias_type.dimensionCount = 1;
    uint32_t bias_type_dims[1] = {depth_out};
    bias_type.dimensions = bias_type_dims;

    // int_scalar_type: for all int32 scalar params, like stride, padding, padding scheme
    ANeuralNetworksOperandType int_scalar_type;
    int_scalar_type.type = ANEURALNETWORKS_INT32;
    int_scalar_type.scale = 0.f;    // These fields are used for quantized tensors
    int_scalar_type.zeroPoint = 0;  // These fields are used for quantized tensors
    int_scalar_type.dimensionCount = 0;
    int_scalar_type.dimensions = nullptr;

    // bool_scalar_type: for bool scalar param: is NCHW
    ANeuralNetworksOperandType bool_scalar_type;
    bool_scalar_type.type = ANEURALNETWORKS_BOOL;
    bool_scalar_type.scale = 0.f;    // These fields are used for quantized tensors
    bool_scalar_type.zeroPoint = 0;  // These fields are used for quantized tensors
    bool_scalar_type.dimensionCount = 0;
    bool_scalar_type.dimensions = nullptr;

    ANeuralNetworksOperandType output_type;
    output_type.type = ANEURALNETWORKS_TENSOR_FLOAT32;
    output_type.scale = 0.f;    // These fields are used for quantized tensors
    output_type.zeroPoint = 0;  // These fields are used for quantized tensors
    output_type.dimensionCount = 4;
    uint32_t output_type_dims[4] = {batches, out_height, out_width, depth_out};
    output_type.dimensions = output_type_dims;


    /*
    add operands to the model
    */
    ANeuralNetworksModel_addOperand(model, &input_type);  // operand 0: input tensor
    ANeuralNetworksModel_addOperand(model, &filter_type);  // operand 1: filter tensor
    ANeuralNetworksModel_addOperand(model, &bias_type);  // operand 2: bias tensor
    if (is_explicit_padding) {
      ANeuralNetworksModel_addOperand(model, &int_scalar_type);  // operand 3: padding: left
      ANeuralNetworksModel_addOperand(model, &int_scalar_type);  // operand 4: padding: right
      ANeuralNetworksModel_addOperand(model, &int_scalar_type);  // operand 5: padding: top
      ANeuralNetworksModel_addOperand(model, &int_scalar_type);  // operand 6: padding: bottom

      ANeuralNetworksModel_addOperand(model, &int_scalar_type);  // operand 7: stride: width
      ANeuralNetworksModel_addOperand(model, &int_scalar_type);  // operand 8: stride: height

      ANeuralNetworksModel_addOperand(model, &int_scalar_type);  // operand 9: fuse code

      ANeuralNetworksModel_addOperand(model, &bool_scalar_type);  // operand 10: is_NCHW: false

      ANeuralNetworksModel_addOperand(model, &int_scalar_type);  // operand 11: dilation: width
      ANeuralNetworksModel_addOperand(model, &int_scalar_type);  // operand 12: dilation: height

      ANeuralNetworksModel_addOperand(model, &output_type);  // operand 13: output tensor
      *operand_cnt += 14;
    }
    else {
      ANeuralNetworksModel_addOperand(model, &int_scalar_type);  // operand 3: padding scheme

      ANeuralNetworksModel_addOperand(model, &int_scalar_type);  // operand 4: stride: width
      ANeuralNetworksModel_addOperand(model, &int_scalar_type);  // operand 5: stride: height

      ANeuralNetworksModel_addOperand(model, &int_scalar_type);  // operand 6: fuse code

      ANeuralNetworksModel_addOperand(model, &bool_scalar_type);  // operand 7: is_NCHW: false

      ANeuralNetworksModel_addOperand(model, &int_scalar_type);  // operand 8: dilation: width
      ANeuralNetworksModel_addOperand(model, &int_scalar_type);  // operand 9: dilation: height

      ANeuralNetworksModel_addOperand(model, &output_type);  // operand 10: output tensor
      *operand_cnt += 11;
    }

    
    /*
    set operand value: copy data from MS tensor
    save the addr of original MS output tensor for result writing
    TODO: to be changed when shifted to multi-op situation: handling output of intermidiate ops
    */
    int ret = SetOperandValue(model);
    if (ret == RET_ERROR) {
      MS_LOG(ERROR) << "NNAPI Set Operand Value error: operation name:" << op_name_;
      return RET_ERROR;
    }


    /*
    add operation
    TODO: in the multi-op situation, the input and output operand indices should be specifically taken care of 
    */
    // int ret;
    if (is_explicit_padding) {
      uint32_t addInputIndexes[13] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12};
      uint32_t addOutputIndexes[1] = {13}; 
      ret = ANeuralNetworksModel_addOperation(
        model,
        ANEURALNETWORKS_CONV_2D,
        13,
        addInputIndexes,
        1,
        addOutputIndexes
      )
    }
    else {
      uint32_t addInputIndexes[10] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
      uint32_t addOutputIndexes[1] = {10}; 
      ret = ANeuralNetworksModel_addOperation(
        model,
        ANEURALNETWORKS_CONV_2D,
        10,
        addInputIndexes,
        1,
        addOutputIndexes
      )
    }

    if (ret != ANEURALNETWORKS_NO_ERROR) {
      MS_LOG(ERROR) << "NNAPI add operation error: operation name:!" << op_name_;
      return RET_ERROR;
    }

    return RET_OK;

  }
  
  
  int ConvolutionNNAPI::SetOperandValue(ANeuralNetworksModel* model) {
    const schema::Conv2DFusion* conv_op = op_primitive_->value_as_Conv2DFusion();
  //1. create Ashared memory of each tensor. Need to calculated the size first.
  //[0] is input, [1] is filter, [2] is bias
    int input_tensor_size = in_tensors_[0].ElementNum() * sizeof(float);
    int filter_tensor_size = in_tensors_[1].ElementNum() * sizeof(float);
    int bias_tensor_size = in_tensors_[2].ElementNum() * sizeof(float);
    int output_tensor_size = out_tensors_[0].ElementNum() * sizeof(float);
    this->input_tensor_size = input_tensor_size;
    this->output_tensor_size = output_tensor_size;
    
    int input_fd = -1;
    int filter_fd = -1;
    int bias_fd = -1;
    int output_fd = -1;

    this->input_tensor = CreateASharedMemory("input_tensor", input_tensor_size, PROT_READ | PROT_WRITE, &input_fd);
    this->filter_tensor = CreateASharedMemory("filter_tensor", filter_tensor_size, PROT_READ, &filter_fd);
    this->bias_tensor = CreateASharedMemory("bias_tensor", bias_tensor_size, PROT_READ, &bias_fd);
    this->output_tensor = CreateASharedMemory("output_tensor", output_tensor_size, PROT_READ | PROT_WRITE, &output_fd);
    // TODO: to check whether these ASharedMemory is nullptr
    this->output_tensor_fd = output_fd;

    fillMemory(input_fd, input_tensor_size, in_tensors_[0].MutableData());
    fillMemory(filter_fd, filter_tensor_size, in_tensors_[1].MutableData());
    fillMemory(bias_fd, bias_tensor_size, in_tensors_[2].MutableData());
    this->output_tensor_mutable = out_tensors_[0].MutableData();
    // fillMemory(input_fd, input_tensor_size, in_tensors_[0].MutableData());

    //2.  set Operand value
    // REMEMBER: don't set the whole graph's input and output!

    // ANeuralNetworksModel_setOperandValueFromMemory(model, 0, this->input_tensor, 0, input_tensor_size); //0: input tensor
    ANeuralNetworksModel_setOperandValueFromMemory(model, 1, this->filter_tensor, 0, filter_tensor_size); //1: filter tensor
    ANeuralNetworksModel_setOperandValueFromMemory(model, 2, this->bias_tensor, 0, bias_tensor_size); //2: bias tensor
    if (is_explicit_padding) {
      ANeuralNetworksModel_setOperandValue(model, 3, &pad_l, sizeof(pad_l));
      ANeuralNetworksModel_setOperandValue(model, 4, &pad_r, sizeof(pad_r));ANeuralNetworksModel_setOperandValue(model, 5, &pad_u, sizeof(pad_u));ANeuralNetworksModel_setOperandValue(model, 6, &pad_d, sizeof(pad_d));

      ANeuralNetworksModel_setOperandValue(model, 7, &stride_width, sizeof(stride_width));ANeuralNetworksModel_setOperandValue(model, 8, &stride_height, sizeof(stride_height));ANeuralNetworksModel_setOperandValue(model, 9, &fuse_code, sizeof(fuse_code));
      ANeuralNetworksModel_setOperandValue(model, 10, &is_NCHW, sizeof(is_NCHW));ANeuralNetworksModel_setOperandValue(model, 11, &dilation_width, sizeof(dilation_width));ANeuralNetworksModel_setOperandValue(model, 12, &dilation_height, sizeof(dilation_height));

      // ANeuralNetworksModel_setOperandValueFromMemory(model, 13, this->output_tensor, 0, output_tensor_size); //13: output tensor
    }
    else {
      ANeuralNetworksModel_setOperandValue(model, 3, &pad_code, sizeof(pad_code));
      ANeuralNetworksModel_setOperandValue(model, 4, &stride_width, sizeof(stride_width));ANeuralNetworksModel_setOperandValue(model, 5, &stride_height, sizeof(stride_height));ANeuralNetworksModel_setOperandValue(model, 6, &fuse_code, sizeof(fuse_code));
      ANeuralNetworksModel_setOperandValue(model, 7, &is_NCHW, sizeof(is_NCHW));ANeuralNetworksModel_setOperandValue(model, 8, &dilation_width, sizeof(dilation_width));ANeuralNetworksModel_setOperandValue(model, 9, &dilation_height, sizeof(dilation_height));

      // ANeuralNetworksModel_setOperandValueFromMemory(model, 10, this->output_tensor, 0, output_tensor_size); //10: output tensor
    }
 
    return RET_OK; //TODO: add not ok test

  }


} // namespace mindspore::lite