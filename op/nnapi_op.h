/**
 * Copyright 2020-2021 Huawei Technologies Co., Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef MINDSPORE_LITE_SRC_RUNTIME_DELEGATE_NNAPI_OP_
#define MINDSPORE_LITE_SRC_RUNTIME_DELEGATE_NNAPI_OP_

#include <utility>
#include <string>
#include <vector>
#include <unistd.h>
#include <memory>
#include <sys/mman.h>

#include <android/NeuralNetworks.h>
#include <android/sharedmem.h>

#include "include/api/kernel.h"
#include "src/common/log_adapter.h"
#include "include/errorcode.h"
// #include "include/graph/graph.h"
// #include "schema/model_generated.h"
// #include "include/api/types.h"
// #include "include/api/data_type.h"

namespace mindspore::lite {
class NNAPIOp {
public:
  explicit NNAPIOp(const schema::Primitive *primitive, 
                      std::vector<mindspore::MSTensor> in_tensors,
                      std::vector<mindspore::MSTensor> out_tensors, 
                      std::string name)
      : op_primitive_(primitive),
        in_tensors_(std::move(in_tensors)),
        out_tensors_(std::move(out_tensors)),
        op_name_(std::move(name)) {
    if (primitive != nullptr) {
      this->type_ = primitive->value_type();
    }
  }


  virtual ~NNAPIOp() = default;

  // Each Op to implement its own version of IsSupport
  virtual int IsSupport(const schema::Primitive *primitive, const std::vector<mindspore::MSTensor> &in_tensors,
                        const std::vector<mindspore::MSTensor> &out_tensors) = 0;
                
  virtual int AddOperand(ANeuralNetworksModel* model, int *operand_cnt) = 0;

  virtual int SetOperandValue(ANeuralNetworksModel* model) = 0;

  // below: to be deleted
  virtual ANeuralNetworksMemory* GetInputTensorMemory() = 0;
  virtual ANeuralNetworksMemory* GetOutputTensorMemory() = 0;
  virtual int GetInputTensorSize() = 0;
  virtual int GetOutputTensorSize() = 0;
  virtual int GetOutputTensorFD() = 0;
  virtual void *GetOutputTensorMutable() = 0;


  // below: already implemented
  ANeuralNetworksMemory* CreateASharedMemory(const char* name, uint32_t size, int prot, int *ret_fd);

  void fillMemory(int fd, uint32_t size, void *tensor_data);

  std::vector<mindspore::MSTensor> &inputs();

  std::vector<mindspore::MSTensor> &outputs();

  const std::vector<NNAPIOp *> &in_ops() const;

  const std::vector<NNAPIOp *> &out_ops() const;

  void set_in_ops(const std::vector<NNAPIOp *> &in_ops);

  void set_out_ops(const std::vector<NNAPIOp *> &out_ops);

  std::string GetOpName();

  schema::PrimitiveType type() const;


protected:
  // std::vector<nvinfer1::ILayer *> layers_;

  const schema::Primitive *op_primitive_;

  std::vector<mindspore::MSTensor> in_tensors_;

  std::vector<mindspore::MSTensor> out_tensors_;

  // std::vector<nvinfer1::ITensor *> tensorrt_in_tensors_;

  // std::vector<nvinfer1::ITensor *> tensorrt_out_tensors_;

  std::vector<NNAPIOp *> in_ops_;

  std::vector<NNAPIOp *> out_ops_;

  std::string op_name_;

  schema::PrimitiveType type_ = schema::PrimitiveType_NONE;
};



template <class T>
NNAPIOp *GetNNAPIOp(const schema::Primitive *primitive, 
                    const std::vector<mindspore::MSTensor> &in_tensors, 
                    const std::vector<mindspore::MSTensor> &out_tensors, 
                    const std::string &name) {
  auto *op = new (std::nothrow) T(primitive, in_tensors, out_tensors, name);
  if (op == nullptr) {
    MS_LOG(ERROR) << "NNAPI op creation failed.";
    return nullptr;
  }

  auto ret = op->IsSupport(primitive, in_tensors, out_tensors);
  if (ret != RET_OK) {
    MS_LOG(ERROR) << "NNAPI op is not supported.";
    delete op;
    return nullptr;
  }
  return op;
}

} // namespace mindspore::lite
#endif  // MINDSPORE_LITE_SRC_RUNTIME_DELEGATE_NNAPI_OP_