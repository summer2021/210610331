/**
 * Copyright 2021 Huawei Technologies Co., Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef MINDSPORE_LITE_SRC_NNAPI_OP_CONVOLUTION_NNAPI_H_
#define MINDSPORE_LITE_SRC_NNAPI_OP_CONVOLUTION_NNAPI_H_

#include "src/delegate/nnapi/op/nnapi_op.h"

namespace mindspore::lite {
class ConvolutionNNAPI : public NNAPIOp {
public:
  ConvolutionNNAPI(const schema::Primitive *primitive, const std::vector<mindspore::MSTensor> &in_tensors,
                      const std::vector<mindspore::MSTensor> &out_tensors, const std::string &name)
      : NNAPIOp(primitive, in_tensors, out_tensors, name) {}

  ~ConvolutionNNAPI() = default;

//   int AddInnerOp(nvinfer1::INetworkDefinition *network) override;

  int IsSupport(const schema::Primitive *primitive, const std::vector<mindspore::MSTensor> &in_tensors,
                const std::vector<mindspore::MSTensor> &out_tensors) override;

  int AddOperand(ANeuralNetworksModel* model, int *operand_cnt) override;

  int SetOperandValue(ANeuralNetworksModel* model) override;

  //TODO: these functions and corresponding attributes should be replaced by logics in nnapi_subgraph module that deals with the input and output of the whole graph.
  // why these functions are written in nnapi_op.h and overridden here: cuz the main module that uses this provisional solution only includes nnapi_op.h!
  ANeuralNetworksMemory* GetInputTensorMemory() override {return input_tensor;}
  ANeuralNetworksMemory* GetOutputTensorMemory() override {return output_tensor;}
  int GetInputTensorSize() override {return input_tensor_size;}
  int GetOutputTensorSize() override {return output_tensor_size;}
  int GetOutputTensorFD() override {return output_tensor_fd;}
  void *GetOutputTensorMutable() override {return output_tensor_mutable;}


private:
//   void SetAttributes(const schema::Conv2DFusion *ms_op, nvinfer1::IConvolutionLayer *current_layer_);

  // input and weight tensors
  // const mindspore::MSTensor *input_tensor;
  // const mindspore::MSTensor *filter_tensor;
  // const mindspore::MSTensor *bias_tensor;
  bool is_explicit_padding;
  // below: padding info
  int pad_u = -1;
  int pad_d = -1;
  int pad_l = -1;
  int pad_r = -1;
  int pad_code = -1;
  int stride_width;
  int stride_height;
  int fuse_code;
  uint8_t is_NCHW = 0;
  int dilation_width;
  int dilation_height;

  // output tensor
  // const mindspore::MSTensor *output_tensor;

  // shared memories
  ANeuralNetworksMemory* input_tensor;
  ANeuralNetworksMemory* filter_tensor;
  ANeuralNetworksMemory* bias_tensor;
  ANeuralNetworksMemory* output_tensor;

  int input_tensor_size;
  int output_tensor_size;
  int output_tensor_fd;
  void *output_tensor_mutable;


//   float *pack_weight_{nullptr};
};
}  // namespace mindspore::lite
#endif  // MINDSPORE_LITE_SRC_NNAPI_OP_CONVOLUTION_NNAPI_H_
