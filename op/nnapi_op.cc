/**
 * Copyright 2020-2021 Huawei Technologies Co., Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "src/delegate/nnapi/op/nnapi_op.h"


namespace mindspore::lite {
// const schema::Primitive *NNAPIOp::GetPrimitive() { return this->op_primitive_; }

// void NNAPIOp::AddInnerInTensors(nvinfer1::ITensor *tensor) { this->NNAPI_in_tensors_.push_back(tensor); }

// void NNAPIOp::AddInnerOutTensors(nvinfer1::ITensor *tensor) { this->NNAPI_out_tensors_.push_back(tensor); }

// std::vector<nvinfer1::ITensor *> &NNAPIOp::GetInnerOutTensor() { return this->NNAPI_out_tensors_; }

// std::vector<nvinfer1::ITensor *> &NNAPIOp::GetInnerInTensors() { return this->NNAPI_in_tensors_; }
ANeuralNetworksMemory* NNAPIOp::CreateASharedMemory(const char* name, uint32_t size, int prot, int *ret_fd) {
  int fd = ASharedMemory_create(name, size * sizeof(float));

  // Create an ANeuralNetworksMemory object from the corresponding ASharedMemory objects.
  ANeuralNetworksMemory* memory = nullptr;
  int ret = ANeuralNetworksMemory_createFromFd(size * sizeof(float),
                                                      prot,
                                                      fd,
                                                      0,
                                                      &memory);
  if (ret != ANEURALNETWORKS_NO_ERROR) {
    MS_LOG(ERROR) << "NNAPI op CreateASharedMemory createFromFd() failed! op name: " << name;
    close(fd);
    return nullptr;
  }
  
  *ret_fd = fd;
  return memory;
}

void NNAPIOp::fillMemory(int fd, uint32_t size, void *tensor_data) {
  // Set the values of the memory.
  // In reality, the values in the shared memory region will be manipulated by
  // other modules or processes.
  void* start_addr = mmap(nullptr,
                        size,
                        PROT_READ | PROT_WRITE,
                        MAP_SHARED,
                        fd,
                        0
                        );
  
  memcpy(start_addr, tensor_data, size);
//   std::fill(data, data + size, value);
  munmap(start_addr, size);
}


std::string NNAPIOp::GetOpName() { return this->op_name_; }

std::vector<mindspore::MSTensor> &NNAPIOp::inputs() { return this->in_tensors_; }

std::vector<mindspore::MSTensor> &NNAPIOp::outputs() { return this->out_tensors_; }

schema::PrimitiveType NNAPIOp::type() const { return this->type_; }

void NNAPIOp::set_in_ops(const std::vector<NNAPIOp *> &in_ops) { this->in_ops_ = in_ops; }

void NNAPIOp::set_out_ops(const std::vector<NNAPIOp *> &out_ops) { this->out_ops_ = out_ops; }

const std::vector<NNAPIOp *> &NNAPIOp::in_ops() const { return this->in_ops_; }

const std::vector<NNAPIOp *> &NNAPIOp::out_ops() const { return this->out_ops_; }
}  // namespace mindspore::lite