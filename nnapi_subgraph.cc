/**
 * Copyright 2021 Huawei Technologies Co., Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "src/delegate/nnapi/nnapi_subgraph.h"
#include <string>
#include <vector>
#include <set>

namespace mindspore::lite {

NNAPISubGraph::~NNAPISubGraph() {
  // Cleanup
//   ANeuralNetworksCompilation_free(compilation);
  ANeuralNetworksModel_free(model);
//   ANeuralNetworksMemory_free(mem1);
// TODO: cleanup the shared memories of every op: for now, only clean up the shared memory of the current op
}

int NNAPISubGraph::Prepare() {
  
  
}


int NNAPISubGraph::Execute() {
  // Compile the model
  ANeuralNetworksCompilation* compilation;
  ANeuralNetworksCompilation_create(model, &compilation);

  //TODO: for the next part of OSPP, try to specify the device to run on 
  ANeuralNetworksCompilation_finish(compilation);

  // Run the compiled model against a set of inputs
  ANeuralNetworksExecution* run1 = nullptr;
  ANeuralNetworksExecution_create(compilation, &run1);

  // Specify where your app reads the input values for the computation. Your app can read input values from
  // either a user buffer or an allocated memory space by calling ANeuralNetworksExecution_setInput() or 
  // ANeuralNetworksExecution_setInputFromMemory() respectively.
  // Set the single input to our sample model. Since it is small, we won't use a memory buffer
//   int ANeuralNetworksExecution_setInputFromMemory(
//   ANeuralNetworksExecution *execution,
//   int32_t index,
//   const ANeuralNetworksOperandType *type,
//   const ANeuralNetworksMemory *memory,
//   size_t offset,
//   size_t length
// )
  ANeuralNetworksExecution_setInputFromMemory(run1, 0, nullptr, this->input_tensor, 0, this->input_tensor_size);

  // Specify where your app writes the output values. Your app can write output values to either a user 
  // buffer or an allocated memory space, by calling ANeuralNetworksExecution_setOutput() or 
  // ANeuralNetworksExecution_setOutputFromMemory() respectively.
  ANeuralNetworksExecution_setOutputFromMemory(run1, 0, nullptr, this->output_tensor, 0, this->output_tensor_size);


  // Starts the work. The work proceeds asynchronously
  ANeuralNetworksEvent* run1_end = nullptr;
  ANeuralNetworksExecution_startCompute(run1, &run1_end);

  // For our example, we have no other work to do and will just wait for the completion
  ANeuralNetworksEvent_wait(run1_end);
  

  // write the result in this->output_tensor memory region back to output MS tensor
  void *outputptr = mmap(
    nullptr,
    this->output_tensor_size,
    PROT_READ,
    MAP_SHARED,
    this->output_tensor_fd,
    0
  );
  
  memcpy(this->output_tensor_mutable, outputptr, this->output_tensor_size);
//   std::fill(data, data + size, value);
  munmap(outputptr, this->output_tensor_size);

  // final free
  ANeuralNetworksEvent_free(run1_end);
  ANeuralNetworksExecution_free(run1);

  //TODO: free compilation; close fd in destructor

  return RET_OK; //TODO：add not ok situations
}


int NNAPISubGraph::Init() {
    int ret = ANeuralNetworksModel_create(&model);
    if (ret != ANEURALNETWORKS_NO_ERROR) {
      MS_LOG(ERROR) << "NNAPI model creation failed.";
      return RET_ERROR;
    }

    return RET_OK;
}


int NNAPISubGraph::BuildNNAPIGraph() {
  /* 
    build NNAPI subgraph with :
    - ops of the subgraph:                operators that defines the NNAPI model. Each Op object contains the fixed param info of the op(layer)
    - input tensors of the subgraph:      input of the execution instance
    - output tensors of the subgraph:     output of the execution instance

    what to do in this function:
    1. copy op params(weight, bias) in MS tensor format to NNAPI-accessible location: first try to do in Prepare()
    2. copy input and output tensor of the graph in MS tensor format to NNAPI-accessible location: first try to do in Prepare()
    3. build the NNAPI graph using ops

    only consider single-conv-layer network first. 

    task:
    1. identify conv layer Operand Type and add operand
    2. add operation
  */ 

  int operand_cnt = 0;
  for (auto op: all_ops_) {
      int ret = op->AddOperand(model, &operand_cnt); // TODO: the way how operand_cnt is added should be changed in multi-op situation: the output of previous op is one of the outputs of the next op!
      if (ret == RET_ERROR) {
        MS_LOG(ERROR) << "NNAPI add operands and operations process failed.";
        return RET_ERROR;
      }

      this->input_tensor = op->GetInputTensorMemory();
      this->output_tensor = op->GetOutputTensorMemory();
      this->input_tensor_size = op->GetInputTensorSize();
      this->output_tensor_size = op->GetOutputTensorSize();
      this->output_tensor_fd = op->GetOutputTensorFD();
      this->output_tensor_mutable = op->GetOutputTensorMutable();

  }

  /*
  TODO: when shifted to the multi-op situation, the way to get the indices of the input and output operands of the whole subgraph should be modified

  */
  output_operand_index = operand_cnt - 1;
  uint32_t modelInputIndexes[1] = {0};
  uint32_t modelOutputIndexes[1] = {operand_cnt - 1};
  int ret = ANeuralNetworksModel_identifyInputsAndOutputs(model, 1, modelInputIndexes, 1, modelOutputIndexes);
  if (ret != ANEURALNETWORKS_NO_ERROR) {
    MS_LOG(ERROR) << "NNAPI model identifyInputsAndOutputs process failed.";
    return RET_ERROR;
  }

  // finish creation
  int ret = ANeuralNetworksModel_finish(model);
  if (ret != ANEURALNETWORKS_NO_ERROR) {
    MS_LOG(ERROR) << "NNAPI model finish process failed.";
    return RET_ERROR;
  }

  return RET_OK;
}



} // namespace mindspore::lite