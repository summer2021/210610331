/**
 * Copyright 2021 Huawei Technologies Co., Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "src/delegate/nnapi/nnapi_delegate.h"
#include "src/delegate/nnapi/op/nnapi_op.h"

#include <vector>
#include <fstream>
#include <string>
#include <queue>
#include "include/errorcode.h"
#include "src/delegate/delegate_utils.h"

namespace mindspore::lite {
  int NNAPIDelegate::Init() {
    // set device info 
    op_func_lists_ = {
      {schema::PrimitiveType_Conv2DFusion, GetNNAPIOp<ConvolutionNNAPI>},
    };

    return RET_OK;
  }
  
  int NNAPIDelegate::Build(DelegateModel *model) {
    KernelIter from, end;
    std::vector<NNAPIOp *> nnapi_ops;
    int graph_index = 0;
    for (KernelIter iter = model->BeginKernelIterator(); iter != model->EndKernelIterator(); iter++) {
      kernel::Kernel *kernel = *iter;
      auto nnapi_op = FindNNAPIOp(kernel, model->GetPrimitive(kernel));
      if (nnapi_op != nullptr) {
        // If nnapi_ops does not equal nullptr, this kernel can be supported by delegate
        if (nnapi_ops.size() == 0) {
          from = iter;
        }
        nnapi_ops.push_back(nnapi_op);
        end = iter;
      } else {
        if (nnapi_ops.size() > 0) {
          auto nnapi_subgraph = CreateNNAPIGraph(nnapi_ops, model, from, end);
          if (nnapi_subgraph == nullptr) {
            MS_LOG(ERROR) << "Create NNAPI Graph failed.";
            return RET_ERROR;
          }
          nnapi_subgraph->set_name("NNAPIGraph" + std::to_string(graph_index++));
          iter = model->Replace(from, end + 1, nnapi_subgraph);
          nnapi_ops.clear();
        }
      }
    }  

    if (nnapi_ops.size() > 0) {
      auto nnapi_subgraph = CreateNNAPIGraph(nnapi_ops, model, from, end);
      if (nnapi_subgraph == nullptr) {
        MS_LOG(DEBUG) << "Create NNAPI Graph failed.";
        return RET_ERROR;
      }
      nnapi_subgraph->set_name("NNAPIGraph" + std::to_string(graph_index++));
      model->Replace(from, end + 1, nnapi_subgraph);
      nnapi_ops.clear();
    }

    return RET_OK;
  }

  NNAPIOp *NNAPIDelegate::FindNNAPIOp(kernel::Kernel *kernel, const schema::Primitive *primitive) {
    // check if the kernel(op) exists in op_func_lists_
    auto in_tensors = kernel->inputs();
    auto out_tensors = kernel->outputs();
    auto name = kernel->name();
    auto node_type = primitive->value_type();
    if (op_func_lists_.find(node_type) != op_func_lists_.end()) {
      return op_func_lists_[node_type](primitive, in_tensors, out_tensors, name); //exists, and call to init
    } else {
      MS_LOG(WARNING) << "Unsupported op type for NNAPI. kernel->name:" << kernel->name()
                      << " type:" << schema::EnumNamePrimitiveType(primitive->value_type());
      return nullptr;
    }
  }

  NNAPISubGraph *NNAPIDelegate::CreateNNAPIGraph(const std::vector<NNAPIOp *> &ops, DelegateModel *model, KernelIter from, KernelIter end) {
    // get the in and out tensors for the subgraph
    auto in_tensors = GraphInTensors<NNAPIOp>(ops, model, from, end);
    auto out_tensors = GraphOutTensors<NNAPIOp>(ops, model, from, end);

    /* 
    build NNAPI subgraph with :
    - ops of the subgraph:                operators that defines the NNAPI model. Each Op object contains the fixed param info of the op(layer)
    - input tensors of the subgraph:      input of the execution instance
    - output tensors of the subgraph:     output of the execution instance
    - context of this mindspore session:  only used to init the "kernel" base class of NNAPISubgraph, practical effect unknown
    - device_info: to be uncovered about what it is: in TensorRT delegate code, used to check whether FP16 inference is enabled
    */
    auto *nnapi_graph = new (std::nothrow) NNAPISubGraph(ops, in_tensors, out_tensors, context_, device_info_);
    if (nnapi_graph == nullptr) {
      MS_LOG(ERROR) << "New nnapi_graph creation failed.";
      return nullptr;
    }

    // statements below are currently inheritted from TensorRT delegate code,
    // and are not definitely kept to use

    // 1. For every op, find pre and next ops
    FindPreNextOps<NNAPIOp>(ops);
    
    // 2. Init NNAPI SubGraph.
    auto ret = nnapi_graph->Init();
    if (ret != RET_OK) {
      MS_LOG(ERROR) << "NNAPIGraph init failed.";
      return nullptr;
    }
   
    // 3. Build NNAPI Model.
    ret = nnapi_graph->BuildNNAPIGraph();
    if (ret != RET_OK) {
      MS_LOG(ERROR) << "NNAPIGraph build failed.";
      return nullptr;
    }
 
   return nnapi_graph;
  }
} // namespace mindspore::lite